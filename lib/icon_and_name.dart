import 'package:flutter/material.dart';

class IconAndName extends StatelessWidget {
  final IconData iconData;
  final String iconName;

  const IconAndName({Key? key, required this.iconData, required this.iconName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: Icon(
                iconData,
                size: 80.0,
              ),
            )
        ),
        Expanded(
            child: Text(iconName),
            flex: 3,
        ),
        // SizedBox(
        //   width: 100.0,
        //   height: 100.0,
        //   child: Icon(
        //     iconData,
        //     size: 80.0,
        //   ),
        // ),
        // Text(iconName),
      ],
    );
  }
}
