import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'constrained_box_example.dart';
import 'icon_and_name.dart';
import 'icon_and_name_column.dart';
import 'login_page.dart';
import 'pricing_details.dart';

void main() {
  runApp(const ApectRatioApp());
}

class ApectRatioApp extends StatelessWidget {
  const ApectRatioApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Responsive and adaptive UI in Flutter',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text('Row Widget'),//Text('Column Widget'),//Text('Flexible'), //Text('ExpandedWidget'),//Text('ConstrainedBox')
          ),
          body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 600.0),
                child: Card(
                  color: Colors.green[200],
                  child: Row(
                    children: [
                      Container(
                        color: Colors.green[500],
                        child: const FlutterLogo(size: 150.0)),
                          Flexible(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: const [
                                PricingDetails(),
                                Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text(
                                    'Description: the Flutter logo is a beautiful blue icon.',
                                    style: TextStyle(fontSize: 18.0)),
                                )
                              ],
                            ),
                          ),
                      ],
                    ),
                  ),
              ),
            ),
          ),

          // const SafeArea(
          //   child: LoginPage(), //IconAndNameColumn(iconData: Icons.web, iconName: 'web'),
          // ),
          // Column(
          //   children: const [
            //   IconAndName(iconData: Icons.web, iconName: 'web'),
            //   IconAndName(iconData: Icons.house, iconName: 'house'),
            //   IconAndName(iconData: Icons.dashboard, iconName: 'dashboard'),
            // ],
          // ),//ConstrainedBoxExample(),
        ),
      ),
    );
  }
}