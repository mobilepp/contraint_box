import 'package:flutter/material.dart';

class PricingDetails extends StatelessWidget {
  const PricingDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: const [
          Text(
            'Price:',
            style: TextStyle(fontSize: 36.0),
          ),
          Spacer(flex:1),
          Text(
            '\$44',
            style: TextStyle(fontSize: 36.0, fontWeight: FontWeight.bold),
          ),
          Spacer(flex:5),
        ],
      ),
    );
  }
}
