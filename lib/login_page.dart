import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 300.0),
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            //const SizedBox(height: 50.0),
            const Spacer(flex: 10),
            const Expanded(flex:10, child: const FlutterLogo(size: 100.0)),
            //const SizedBox(height: 100.0),
            const Spacer(flex: 3),
            const TextField(decoration: InputDecoration(labelText: 'Username')),
            const TextField(decoration: InputDecoration(labelText: 'Password')),
            //const SizedBox(height: 30.0),
            const Spacer(flex: 8),
            ElevatedButton(onPressed: () {}, child: const Text('Login')),
            const Spacer(flex: 20),
          ],
        ),
      ),
    );
  }
}
